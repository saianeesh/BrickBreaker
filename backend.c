#include <stdlib.h>
#include <ncurses.h>
#include "objs.h"

struct objs *head = NULL;

void
bounce(int type)
{
    switch (type) {
        case COLLISION_BRICK:
        case COLLISION_Y:
            dy *= -1;
            break;
        case COLLISION_X:
            dx *= -1;
            break;
    }
}

static int
collide_brick(const struct objs *ball)
{
    // current x and y of BALL equal to any BRICK x and y
    //struct objs *curr = head
    struct objs *prev;
    for (struct objs *curr = head; curr != NULL; curr = curr->next) {
        if (curr->x == ball->x && curr->y == ball->y) {  
            // collides
            // removes brick from linked list
            prev->next = curr->next; // curr is removed from the list
            if (curr == head)
                head = curr->next;
            free(curr);
            return COLLISION_BRICK;
        }
        prev = curr;
    }
    /*
    while (curr != NULL) {
        if (curr->x == ball->x && curr->y == ball->y) {  
            // collides
            // removes brick from linked list
            prev->next = curr->next; // curr is removed from the list
            if (curr == head)
                head = curr->next;
            free(curr);
            return COLLISION_BRICK;
        }

        prev = curr;
        curr = curr->next;
    }*/
    return NO_COLLISION;
}

static int
collide_wall(int maxy, int maxx, const struct objs *ball)
{
    if (ball->x >= (maxx) || ball->x <= 0) {
        return COLLISION_X;
    } else if (ball->y <= 0) {
        return COLLISION_Y;
    } else if (ball->y >= maxy) {
        return QUIT;
    }
   
    return NO_COLLISION; 
}

void
init_bricks(void)
{
    int x = 5, y = 5;
    for (int i = 0; i < NBRICKS; i++, x++) {
        struct objs *brick = (struct objs *) malloc(sizeof(struct objs));
        brick->icon = ACS_BLOCK;
        brick->breakable = TRUE;
        brick->x = x;
        brick->y = y;
        brick->next = head;
        
        head = brick;
    }

}

int
collide(int maxy, int maxx, const struct objs *ball)
{
    // some process that uses
    // collide_wall and collide_brick
    int collision_brick = collide_brick(ball);
    int collision_wall = collide_wall(maxy, maxx, ball);
    if (collision_brick == NO_COLLISION && collision_wall == NO_COLLISION) {
        return NO_COLLISION;
    } else if (collision_brick != NO_COLLISION) {
        return collision_brick;
    } else {
        return collision_wall;
    }
}


int
move_slider(int maxx, struct objs *slider)
{
    int ch = getch();

    switch(ch) {
        case KEY_LEFT:
            if (slider->x - 1 < 0) {
                return slider->x;
            } else {
                return --(slider->x);
            }
            break;
        case KEY_RIGHT:
            if (slider->x + SLIDER_LENGTH + 1 > maxx) {
                return slider->x;
            } else {
                return ++(slider->x);
            }
            break;
        default:
            return slider->x;
    }

}


int
collide_slider(const struct objs *slider, const struct objs *ball)
{
    if (ball->x > slider->x
            && ball->x < slider->x + SLIDER_LENGTH
            && ball->y == slider-> y) {
        return COLLISION_Y;
    }
    return NO_COLLISION;
}


int
number_of_bricks(void)
{
    int numOfBricks = 0;
    for (struct objs *curr = head; curr != NULL; curr = curr->next) {
        numOfBricks++;
    }

    return numOfBricks;
}


