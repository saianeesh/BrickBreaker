CC = gcc
CFLAGS = -Wall -pedantic
CFLAGS += -I. -lncurses

SRC = $(wildcard *.c)
OBJ = $(patsubst %.c,%.o,$(SRC))
HEADERS = $(wildcard *.h)
TARGET = breakout

.PHONY : clean all
.DEFAULT_GOAL: all

all: $(TARGET)

$(TARGET) : $(OBJ) $(HEADERS)
	$(CC) -o $@ $(CFLAGS) $^

clean : 
	rm -rf *.o $(TARGET)



